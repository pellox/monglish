package info.pello.monglish;

import java.io.IOException;

/**
 * Main class where web server is fired
 * @author Pello Altadill
 * @greetz 4 u
 */
public class Monglish {

	public static void main(String[] args) throws IOException {
		try {
	       if (args.length == 0) {
	            new MonglishController("mongodb://localhost");
	        }
	        else {
	            new MonglishController(args[0]);
	        }
		} catch (Exception e) {
			System.out.println("Error booting monglish: " + e.getMessage());
		}
	}

}
