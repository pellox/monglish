/**
 * 
 */
package info.pello.monglish;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author luser
 *
 */
public class StringUtilsTest {

	private StringUtils stringUtils;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		stringUtils = new StringUtils();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link info.pello.monglish.StringUtils#generatePermalink(java.lang.String)}.
	 */
	@Test
	public void testGeneratePermalink() {
		String target = stringUtils.generatePermalink("Hello World!");
		String expected = "hello_world";
		assertEquals("Permalink generation is correct",target, expected);
	}

}
