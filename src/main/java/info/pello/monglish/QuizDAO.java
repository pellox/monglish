package info.pello.monglish;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;

import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

public class QuizDAO {
    private final DBCollection quizCollection;
    private Random random = new SecureRandom();

    /**
     * Constructor
     * @param blogDatabase
     */
    public QuizDAO(final DB blogDatabase) {
        quizCollection = blogDatabase.getCollection("quizzes");
    }

    /**
     * adds quiz into database
     * @param name
     * @param options
     * @param clue
     * @param correct
     * @return
     */
    // validates that username is unique and insert into db
    public boolean addQuiz(String question, String[] options, String clue, String answer) {
    	StringUtils stringUtils = new StringUtils();
        System.out.println("inserting quiz entry " + question);

        
        BasicDBObject quiz = new BasicDBObject("title", question);
        quiz.append("options", options);
        quiz.append("permalink", stringUtils.generatePermalink(question));
        quiz.append("clue", clue);
        quiz.append("answer", answer);

        try {
        	quizCollection.insert(quiz);
            return true;
        } catch (MongoException.DuplicateKey e) {
            System.out.println("Question already in use: " + question);
            return false;
        }
    }


    /**
     * find quiz by _id
     * @param permalink
     * @return
     */
    public DBObject findById(String _id) {
        DBObject quiz = quizCollection.findOne(new BasicDBObject("_id", _id));
        return quiz;
    }

    
/*
    public List<DBObject> findByAttemptId (int attemptId) {
        List<DBObject> posts;
        BasicDBObject query = new BasicDBObject("tags", tag);
        System.out.println("/tag query: " + query.toString());
        DBCursor cursor = quizCollection.find(query).sort(new BasicDBObject().append("date", -1)).limit(10);
        try {
            posts = cursor.toArray();
        } finally {
            cursor.close();
        }
        return posts;
    }
 */ 




}
