package info.pello.monglish;

/**
 * some useful utilities for Strings
 * @author Pello Altadill
 *
 */
public class StringUtils {
	
	private String string;
	
	/**
	 * default constructor
	 */
	public StringUtils () {
		
	}
	
	
	/**
	 * generates permalink
	 * @param string
	 * @return
	 */
	public String generatePermalink (String string) {

        String permalink = string.replaceAll("\\s", "_"); 
        permalink = permalink.replaceAll("\\W", ""); 
        permalink = permalink.toLowerCase();
        
        return permalink;
	}


}
